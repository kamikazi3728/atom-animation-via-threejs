var orbitals = [];//DO NOT MODIFY
var protons = 92;
var neutrons = 143;
//Modifiable constants at the top for ease of access
const atom = "U235";
setAtom(atom);//UNCOMMENT ONLY IF atom constant is to be used!
const ringDist = {
	min: 2,
	max: 2.9//3.5
};
const nucleusRad = (0.7/Math.sqrt(180))*Math.sqrt(protons);
const ringRotationRate = 2;//4?------------------------------------------
const nucleonSize = 0.15;//0.15
const electronSize = 0.05;
const ringSize = electronSize/10;

//Function to set protons and neutrons based on predefined symbols/names

function setAtom(symbol){
	switch(symbol){
		case "U235":
		case 'U':
		case 'uranium':
			protons = 92;
			neutrons = 143;
			break;
		case "U238":
			protons = 92;
			neutrons = 146;
			break;
		case "C":
		case "carbon":
			protons = 6;
			neutrons = 6;
			break;
		case "O":
		case "oxygen":
			protons = 8;
			neutrons = 8;
			break;
		default:
			protons = 92;
			neutrons = 143;
			break;
	}
}


//Define constants
const suborbitals  = [2,6,10,14,18,22]; //from https://chemistry.stackexchange.com/questions/8598/what-are-the-maximum-number-of-electrons-in-each-shell
const orbitalSizes = [2,8,18,32,50,72]; //From odd addition problem with suborbitals
const maxOrbSizes = [2,10,28,60,110,182];//From odd addition problem with orbitalSizes

//Create scene, camera, and renderer then position the camera
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera( 75, 1/1, 0.1, 1000 );
const renderer = new THREE.WebGLRenderer({
	alpha: true,/*For transparent background*/
	antialias: true,
	canvas: document.getElementById("atomCanvas")
});
camera.position.z = 5;

//Add lighting
const light = new THREE.PointLight( 0xffffff, 1, 100 );//Color,intensity,(distance?)
const light2 = new THREE.PointLight( 0xffffff, 1, 100 );
light.position.set(3,3,2);
light2.position.set(3,-2,0);
scene.add(light);
scene.add(light2);


//Define material colors
const mat = {};
//Protons
mat.red = new THREE.MeshPhongMaterial({color:0xff0000});
//Neutrons
mat.white = new THREE.MeshPhongMaterial({color: 0xffffff });
//Electrons
mat.yellow = new THREE.MeshPhongMaterial({color: 0xffff00 });
//Orbital rings
mat.ring = new THREE.MeshPhongMaterial({color: 0xffffff });

//Create all necessary geometries
const nucleonGeometry = new THREE.SphereGeometry(nucleonSize,10,10);//radius,widthSegments,heightSegments
const electronGeometry = new THREE.SphereGeometry(electronSize,10,10);//radius,widthSegments,heightSegments

//Create all orbitals (Energy levels)
for(var i=0;i<=findOrbitals(protons);i++){
	let eThisOrb = orbitalSizes[i];
	//Account for last orbital
	if(i==findOrbitals(protons)){
		eThisOrb = eThisOrb-(maxOrbSizes[i]-protons);
	}
	orbitals.push({electrons:eThisOrb});
}
//Find all orbital radii and create mesh objects for them
if(orbitals.length==1){
	orbitals[0].ring = createOrbital(ringDist.max,orbitals[0].electrons);
}else{
	let dRing = ((ringDist.max-ringDist.min)/(orbitals.length-1));
	for(var i=0;i<orbitals.length;i++){
		orbitals[i].rad = ringDist.min+(dRing*i);
		orbitals[i].ring = createOrbital(orbitals[i].rad,orbitals[i].electrons);
	}
}
//Add all orbitals to scene at random orientations
orbitals.forEach((orb,k)=>{
	scene.add(orb.ring);
	orb.ring.rotation.y += Math.random()*Math.PI;
	orb.ring.rotation.x += Math.random()*Math.PI;
});

//Create nucleus
const nucleus = createNucleus(nucleusRad,protons,neutrons);
nucleus.position.set(0,0,0);
scene.add(nucleus);

//Size and append the scene to atom.html
//renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setSize(document.body.clientWidth, document.body.clientHeight);
document.body.appendChild(renderer.domElement);


//find number of orbitals (STARTING AT 0) that the atom should have
function findOrbitals(nElectrons){
	for(var i=0;i<maxOrbSizes.length;i++){
		if(nElectrons<=maxOrbSizes[i]){
			return i;
		}
	}
}

//Function for orbital creation
function createOrbital(radius,electrons){
	let group = new THREE.Group();
	group.add(new THREE.Mesh(new THREE.TorusGeometry(radius,ringSize,5,36),mat.ring));
	for(var i=0;i<electrons;i++){
		let tempElectron = new THREE.Mesh(electronGeometry,mat.yellow);
		tempElectron.position.set(radius*Math.sin(Math.PI*2/electrons*i),radius*Math.cos(Math.PI*2/electrons*i),0);
		group.add(tempElectron);
	}
	return group;
}

//function to create the nucleus object, takes nucleus radius, # protons, # neutrons
function createNucleus(radius,p,n){
	//Create a group
	let group = new THREE.Group();
	//Get coordinates of vertices on a rough sphere which altogether number approx. A
	let tempSph = new THREE.SphereGeometry(radius,Math.ceil(Math.sqrt(p+n)),Math.ceil(Math.sqrt(p+n)))
	var coords = tempSph.attributes.position.array;
	let shift = false;
	for(var i=0;i<coords.length/3;i++){
		//If not in the beginning, "shift" (rotate) only in neighboring layers
		if(i>3){
			if(coords[(3*i)+1]!=coords[(3*(i-1))+1]){shift = !shift;}
		}
		//Place nucleons according to the probability a nucleon is a proton, BAD FOR SMALL NUCLEI -------------------------------------------------------------------------------------------
		var newNucleon;
		if(Math.random()<=protons/(protons+neutrons)){
			newNucleon = new THREE.Mesh(nucleonGeometry,mat.red);
		}
		else{
			newNucleon = new THREE.Mesh(nucleonGeometry,mat.white);
		}
		//Change nucleon position around the center axis according to their "plane" to make the nucleus look more evenly dispersed
		if(shift){
			newNucleon.position.set((coords[(3*i)]+coords[(3*(i+1))])/2,coords[(3*i)+1],(coords[(3*i)+2]+coords[(3*(i+1))+2])/2);
		}else{
			newNucleon.position.set(coords[(3*i)],coords[(3*i)+1],coords[(3*i)+2]);
		}
		//Add the new nucleon to the group
		group.add(newNucleon);
	}
	//Return the created group
	return group;
}

//Function for animation frames
const animate = function () {
	requestAnimationFrame( animate );
	
	//Rotate nucleus
	nucleus.rotation.y += -0.01;
	
	//Rotate Orbitals
	orbitals.forEach((orb,i)=>{
		orb.ring.rotation.z += ringRotationRate/(100);	//ROTATION RATE OF ELECTRONS AROUND ORBITALS, other (local) axes below
		//Change rotations based on ring numbers being even or odd
		if(i % 2 == 0){
			orb.ring.rotation.y += (0.01+(i*0.002))/(orb.rad-1.5); 
			orb.ring.rotation.x += (0.01+(i*0.002))/(orb.rad-1.5);
		}else{
			orb.ring.rotation.y += (-0.01-(i*0.002))/(orb.rad-1.5);
			orb.ring.rotation.x += (-0.01-(i*0.002))/(orb.rad-1.5);
		}
	});
	//Render updated scene
	renderer.render(scene,camera);
};
animate();